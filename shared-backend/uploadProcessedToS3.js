const fs = require('fs-extra')
const path = require('path')
const crypto = require('crypto')
const AWS = require('aws-sdk')

process.on('unhandledRejection', (reason, promise) => {
  console.error('Uncaught error in', promise)
  console.error(reason)
  process.exit(1)
})

const bucketName = 'processed-data-cache'

const baseDir = path.resolve(__dirname, '..', 'data')
const processedDir = path.join(baseDir, 'processed')

if (!fs.existsSync(processedDir)) {
  throw new Error('No processed data to upload')
}

// in S3, an ETag is just the MD5 of the file contents
const hashFile = data => crypto.createHash('md5').update(data).digest('hex')

;(async () => {
  const sts = new AWS.STS()
  const role = await sts.assumeRole({
    RoleArn: 'arn:aws:iam::',
    RoleSessionName: '',
  }).promise()
  console.log('Assumed role:', role)

  AWS.config.credentials = new AWS.Credentials(
    role.Credentials.AccessKeyId,
    role.Credentials.SecretAccessKey,
    role.Credentials.SessionToken)

  // it's now safe to set this
  const s3 = new AWS.S3()

  const objects = await s3.listObjectsV2({ Bucket: bucketName }).promise()
  const keyToHashS3 = objects.Contents.reduce((acc, item) => {
    acc[item.Key] = item.ETag.slice(1,-1)
    return acc
  }, {})

  const processedFiles = fs.readdirSync(processedDir)

  // don't upload the last day. it has issues sometimes and is empty. it's a
  // matter of timezones? regardless, don't screw up other neighbouring builds
  // which will pull from S3
  processedFiles.pop()

  // now perform the upload
  processedFiles.forEach(async filename => {
    const content = fs.readFileSync(path.join(processedDir, filename))
    const hash = hashFile(content)
    const prevHash = keyToHashS3[filename]

    // skip duplicates. although, there should never be conflicts for this
    if (prevHash && prevHash === hash) {
      console.log(`Unchanged file; skipping upload for '${filename}'`)
      return
    }

    console.log(
      `Uploading '${filename}'`,
      '\n  New ETag:', hash,
      '\n      Prev:', prevHash || '-')

    await s3.putObject({
      Bucket: bucketName,
      ACL: 'private',
      Key: filename,
      Body: content,
      ContentType: 'application/json; charset=UTF-8',
    }).promise()
  })
})()

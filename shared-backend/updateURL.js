// This file updates a bucket to point to another bucket. Allows people to
// bookmark one URL and have the underlying data change each day

const minimist = require('minimist')
const AWS = require('aws-sdk')

const argv = minimist(process.argv.slice(2))

if (!('bucket' in argv) || !('url' in argv)) {
  throw new Error('Must provide both a url and bucket argument')
}

const conf = {
  IndexDocument: {
    Suffix: 'index.html',
  },
  RoutingRules: [
    {
      Condition: {
        KeyPrefixEquals: '',
      },
      Redirect: {
        HttpRedirectCode: '303',
        HostName: 'the-bucket-website.s3.amazonaws.com',
        Protocol: 'https',
        ReplaceKeyWith: argv.url.split('.com/')[1].trim(),
      },
    },
  ],
}

const s3 = new AWS.S3()
s3.putBucketWebsite({
  Bucket: argv.bucket,
  WebsiteConfiguration: conf,
}).promise()
  .then(() => {
    console.log()
    console.log('Updated', argv.bucket, 'redirection URL')
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })

// creates an HTML catalog of S3 signed URLs to each report in build/reports/.
// uploads them to S3, including the catalog, which has its signed URL logged

const fs = require('fs-extra')
const path = require('path')
const crypto = require('crypto')
const AWS = require('aws-sdk')
const minimist = require('minimist')

const { Body, ListItem } = require('./catalog')

process.on('unhandledRejection', (reason, promise) => {
  console.error('Uncaught error in', promise)
  console.error(reason)
  process.exit(1)
})

const argv = minimist(process.argv.slice(2))
const prefix = argv.prefix
const baseDir = argv.base

if (!prefix || !baseDir) {
  throw new Error(
    'Must provide an S3 upload prefix and base path to a build folder')
}

// in S3, an ETag is just the MD5 of the file contents
const hashFile = data => crypto.createHash('md5').update(data).digest('hex')
const bucketName = 'dashboard-export'
const catalogFilename = 'latest-catalog.html'

const reportDir = path.join(baseDir, 'reports')
if (!fs.existsSync(reportDir)) {
  throw new Error('Nothing to publish; no build/reports/')
}

// assume these exist if the reports/ directory does
const publicFiles =
  ['bundle.js', 'style.css', 'Museo_Slab_100-webfont.woff2']
    .map(x => path.join(baseDir, x))

// it's possible another catalog already exists here, so filter it out
const reportFiles = fs.readdirSync(reportDir).filter(x => x !== catalogFilename)
const reportNames = reportFiles.map(x => x.split('.html')[0])

// already lost file name safety with report names like "United States.html" so
// don't bother making the date name safe either. this has spaces and a comma
const buildDate = (new Date()).toISOString().split('T')[0]

const s3 = new AWS.S3()

console.log('Running with AWS credentials:')
console.log(AWS.config.credentials)

const signedUrl = file =>
  s3.getSignedUrl('getObject', {
    Bucket: bucketName,
    Key: `${prefix}/${buildDate}/${file}`,
    Expires: 86400, // in seconds
  })

// make signed URLs for each of the countries in the catalog
const content = reportNames.reduce((html, reportName) => {
  html += ListItem({
    title: reportName,
    url: signedUrl(reportName + '.html'),
  })
  return html
}, '')

const catalogHtml = Body({ content })
const catalogHtmlMin = catalogHtml.replace(/\n\s{2,}/g, '')

console.log('Final catalog HTML:')
console.log(catalogHtml)

const catalogFile = path.join(baseDir, catalogFilename)
fs.writeFileSync(catalogFile, catalogHtmlMin)

const toUpload = [].concat(
  reportFiles.map(x => path.join(reportDir, x)), publicFiles, catalogFile)

;(async () => {
  console.log('Listing S3 objects')
  const objects = await s3.listObjectsV2({ Bucket: bucketName }).promise()
  console.log('S3 has', objects.Contents.length, 'items')
  const keyToHashS3 = objects.Contents.reduce((acc, item) => {
    acc[item.Key] = item.ETag.slice(1,-1)
    return acc
  }, {})

  // now perform the upload. note this is an async loop
  await Promise.all(toUpload.map(async file => {
    const content = fs.readFileSync(file)
    const hash = hashFile(content)
    const filename = file.split(path.sep).pop()

    const uploadPath = filename.endsWith('woff2')
      ? filename // upload fonts to root
      : `${prefix}/${buildDate}/${filename}`

    // skip duplicates. although, there should never be conflicts for this
    const prevHash = keyToHashS3[uploadPath]
    if (prevHash && prevHash === hash) {
      console.log(`Unchanged file; skipping upload for '${filename}'`)
      return
    }

    const visibilty = publicFiles.includes(file)
      ? 'public-read'
      : 'private'

    console.log(
      `Uploading '${filename}' [${visibilty}] to ${uploadPath}`,
      '\n  New ETag:', hash,
      '\n      Prev:', prevHash || '-')

    const contentType = (() => {
      switch (filename.split('.')[1]) {
        case 'html': return 'text/html'
        case 'css': return 'text/css'
        case 'js': return 'application/javascript'
      }
    })()

    await s3.putObject({
      Bucket: bucketName,
      ACL: visibilty,
      Key: uploadPath,
      Body: content,
      ContentType: contentType + '; charset=UTF-8',
    }).promise()
      .then(res => {
        console.log('Resolved:', res)
      })
      .catch(err => {
        console.log('Failed:', err)
      })
  }))

  console.log()
  console.log('Published. Catalog URL:')
  console.log(signedUrl(catalogFilename))
})()

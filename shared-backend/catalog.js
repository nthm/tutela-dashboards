// this is a module, not a standalone callable file

const Body = ({ content }) => `
  <style>
    @font-face{
      font-family: Slab;
      src: url(/Museo_Slab_100-webfont.woff2) format("woff2")
    }
    body {
      font-family: Slab;
    }
    main {
      max-width: 400px;
      margin: 100px auto;
      padding: 40px;
      box-shadow: 0 4px 8px 0 rgba(0,0,0,.2);
      text-align: center;
      background: #f5f5f5;
    }
    hr {
      background-image: linear-gradient(90deg, transparent, rgba(0,0,0,.75), transparent);
      border: 0;
      height: 1px;
      margin-bottom: 20px;
    }
    article {
      background: #fff;
      border-radius: 2px;
      box-shadow: 0 2px 4px 0 rgba(0,0,0,.2);
      margin-bottom: 15px;
      padding: 10px 20px;
      text-align: left;
    }
    a {
      color: #333;
      text-decoration: none;
    }
  </style>
  <main>
    <h1>Catalog</h1>
    <hr>
    ${content}
  </main>`

const ListItem = ({ title, url }) =>`
  <a href='${url}'>
    <article>
      <h2>${title}</h2>
    </article>
  </a>`

module.exports = {
  Body,
  ListItem,
}

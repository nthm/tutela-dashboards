This suffered from a bit of over-engineering.

Trying to condense three repos to avoid copy/pasting code will do that to you...

In the tree below, a few files are...trying to help, but likely not. For
instance, index.js is just a wrapper to get away with using `esm` (it was 2017
and hadn't landed in Node officially yet). Other files, like catalog.js are only
a place to paste a chunk of code - editor folding is likely a better choice.

The entire wantedFiles/ is only used in one place so having other files is over
kill. It was an attempt to break up code to allow for some abstraction... Hurts
readability in the end.

Also for util/ where a file is one concept or a few LoC.

```
shared-backend/
├── fetch
│   ├── dataserviceFetch.js
│   ├── gSheetFetchKnownPartners.js
│   ├── index.js
│   ├── util
│   │   ├── helpers.js
│   │   ├── requestQueue.js
│   │   └── saveRequest.js
│   └── wantedFiles
│       ├── gSheetAPIKeysToParams.js
│       └── rangeISODates.js
├── process
│   ├── makePartnerMaps.js
│   └── sumRawToProcessed.js
├── catalog.js
├── publish.js
├── updateURL.js
├── uploadProcessedToS3.js
├── example.md
├── readme.md
└── template.md
```

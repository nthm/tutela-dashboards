import fs from 'fs-extra'
import path from 'path'
import minimist from 'minimist'
import fetch from 'node-fetch'
import AWS from 'aws-sdk'

import { size } from './util/helpers'
import { requestQueue } from './util/requestQueue'
import { saveRequest } from './util/saveRequest'

import { rangeISODates } from './wantedFiles/rangeISODates'
import { gSheetAPIKeysToParams } from './wantedFiles/gSheetAPIKeysToParams'

process.on('unhandledRejection', err => { throw err })

const argv = minimist(process.argv.slice(2))

const baseDir = path.resolve(__dirname, '..', '..', 'data')
const resolveRawSaveDir = saveDir => {
  const dir = path.join(baseDir, 'raw', saveDir)
  fs.ensureDirSync(dir)
  return dir
}

// determine which dates are wanted by both Status Dashboard and AppMetrics
// they're compared to the existing files
const now = new Date()
const isoDate = date => date.toISOString().split('T')[0]

now.setDate(now.getDate() - 31)
const thirtyDaysFromYesterday = isoDate(now)
// don't use !('to' in argv) and minimist will turn `--to` into `{ to: true }`
if (!argv.from || argv.from === true) {
  console.log(`'--from' not provided. Setting to '${thirtyDaysFromYesterday}'`)
  argv.from = thirtyDaysFromYesterday
}

now.setDate(now.getDate() + 30)
const yesterday = isoDate(now)
if (!argv.to || argv.to === true) {
  console.log(`'--to' not provided. Setting to '${yesterday}'`)
  argv.to = yesterday
}

const wantedDates = rangeISODates(argv.from, argv.to)

// determine which API keys are wanted by Deployment Tracking
// gSheetAPIKeys will explain the fallback if given [] as parameters
const partners = ('partners' in argv && argv.partners !== true)
  ? argv.partners.split(',')
  : []

const keyToParam = gSheetAPIKeysToParams(partners)
// HACK:
const hackMapping = {}
const wantedAPIKeyFiles = Object.entries(keyToParam).map(([key, name]) => {
  const filename = `${key.substr(0,6)}-${name.replace(/\s+/g, '_')}`
  // this is a hack because information required to make the request,
  // specifically the API key, is lost in translation...
  hackMapping[filename] = key
  return filename
})

const processedDir = path.join(baseDir, 'processed')
fs.ensureDirSync(processedDir)
const existingProcessedFiles = fs.readdirSync(processedDir)

const bucketName = 'processed-data'
let s3 // const s3 = new S3()

const services = [
  {
    name: 'S3 Processed',
    concurrentRequests: 20,
    saveDir: processedDir,
    wantedFiles: wantedDates,
    request(date) {
      return saveRequest({
        requestName: `${this.name} ${date}`,
        requestPromise: s3.getObject({
          Bucket: bucketName,
          Key: `${date}.json`,
        })
          .promise()
          .then(res => JSON.parse(res.Body.toString('utf-8'))),
        saveDir: this.saveDir,
        saveName: date,
      })
    },
  },
  {
    name: 'Status Dashboard API Server',
    url: 'https://>.>tutela.com/>.>',
    // requests limiting depends on Dynamo table throttling
    concurrentRequests: 15,
    saveDir: resolveRawSaveDir('status-dashboard'),
    wantedFiles: wantedDates,
    request(date) {
      return saveRequest({
        requestName: `${this.name} ${date}`,
        requestPromise: fetch(`${this.url}?from=${date}&to=${date}`, {
          headers: {
            Authorization: '>.>',
          },
        }).then(r => r.json()),
        saveDir: this.saveDir,
        saveName: date,
      })
    },
  },
  {
    name: 'Deployment Tracking Server',
    url: 'https://>.>.tutela.com/>.>/>.>',
    concurrentRequests: 5,
    saveDir: resolveRawSaveDir('deployment-tracking'),
    // this is not a list of files. see below
    wantedFiles: wantedAPIKeyFiles,
    request(file) {
      // revive the full API key that was lost in translation earlier
      const key = hackMapping[file]
      return saveRequest({
        requestName: `${this.name} ${key}`,
        requestPromise: fetch(`${this.url}/${key}`, {
          headers: {
            TutelaSecret: '>.>',
          },
        }).then(r => r.json()),
        saveDir: this.saveDir,
        saveName: file,
      })
    },
  },
  {
    name: 'Local API Server',
    url: 'https://>.>.tutela.com/>.>/>.>',
    concurrentRequests: 5,
    saveDir: resolveRawSaveDir('local-server'),
    wantedFiles: wantedDates,
    request(date) {
      return saveRequest({
        requestName: `${this.name} ${date}`,
        requestPromise: fetch(`${this.url}?from=${date}&to=${date}`, {
          headers: {
            Authorization: '>.>',
            CWAK: '>.>',
          },
        })
          .then(r => r.json())
          // avoid writing 15MB payloads a day. drop Jitter/Packet Loss/etc...
          .then(data => data.map(({
            CreatedDate, Name, MetricA, MetricB, Country,
          }) => ({
            CreatedDate, Name, MetricA, MetricB, Country,
          }))),
        saveDir: this.saveDir,
        saveName: date,
      })
    },
  },
]

;(async () => {
  console.log()
  console.log('Looking for pre-processed files in S3')

  const sts = new AWS.STS()
  const role = await sts.assumeRole({
    RoleArn: 'arn:aws:iam::',
    RoleSessionName: '',
  }).promise()
  console.log('Assumed role:', role)

  AWS.config.credentials = new AWS.Credentials(
    role.Credentials.AccessKeyId,
    role.Credentials.SecretAccessKey,
    role.Credentials.SessionToken)

  // it's now safe to set this
  s3 = new AWS.S3()

  const data = await s3.listObjectsV2({ Bucket: bucketName }).promise()
  const existingS3Files = data.Contents.map(item => item.Key)
  console.log('S3 has', existingS3Files.length, 'processed files')
  console.log()

  // NOTE: there might be files on local processed/ that aren't on S3.
  // whatever. leave that up to the sumRawToProcessed script

  // mutate the services by adding a new field
  services.forEach(service => {
    const { name, wantedFiles, saveDir } = service

    let missingFiles
    // HACK:
    if (name === 'S3 Processed') {
      missingFiles = wantedFiles.filter(file =>
        existingS3Files.includes(`${file}.json`) &&
        !existingProcessedFiles.includes(`${file}.json`))
    } else {
      const existingRawFiles = fs.readdirSync(saveDir)

      // only download these if they're __n o w h e r e__ else
      missingFiles = wantedFiles.filter(file =>
        !existingS3Files.includes(`${file}.json`) &&
        !existingProcessedFiles.includes(`${file}.json`) &&
        !existingRawFiles.includes(`${file}.json`))
    }

    console.log(`  - ${name}:`)
    console.log(`    ${saveDir}`)
    missingFiles.forEach(file => { console.log(`    - ${file}`) })
    console.log('\n')
    service.missingFiles = missingFiles
  })

  // array of promises. one for each service's requests
  const servicePromises = services.map(service => {
    const { missingFiles, concurrentRequests } = service
    const request = service.request.bind(service)

    const queue = requestQueue(concurrentRequests)
    // request each missing file by adding it to the queue
    return Promise.all(missingFiles.map(file => queue(request, file)))
  })

  // promise for requests of all services
  await Promise.all(servicePromises)
  console.log('Total JSON written:', size.format(size.total))
})()

import fs from 'fs-extra'
import path from 'path'

import { size } from './helpers'

// note that this is only expects a request promise, and doesn't require you to
// use a specific implementation such as fetch/xhr/requests/http

// log request timing, and save pretty-printed to saveDir/file
const saveRequest = async ({
  requestName, requestPromise, saveDir, saveName,
}) => {
  const prettyTimeLog = `Done: ${requestName}`
  console.time(prettyTimeLog)
  const savePath = path.join(saveDir, `${saveName}.json`)
  try {
    console.log(`Started: ${requestName}`)
    const data = await requestPromise
    fs.writeJSONSync(savePath, data, { spaces: 2 })
  } catch (err) {
    console.error(`Request failed; "${saveName}"`, err)
    return
  } finally {
    console.timeEnd(prettyTimeLog)
  }
  const stats = fs.statSync(savePath)
  size.total += stats.size
  console.log('JSON size:', size.format(stats.size))
}

export { saveRequest }

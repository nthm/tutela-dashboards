// limit concurrent network requests to each API. lifted from `p-limit`

// returns a function that you use as a wrapper for a functions you'd like to
// limit. example usage that will limit to 10 open/unresolved requests at once:

// const limit = requestQueue(10); [0...500].forEach(x => limit(fn, x))

const requestQueue = concurrency => {
  const queue = []
  let active = 0

  const next = () => {
    active--
    if (queue.length > 0) {
      queue.shift()()
    }
  }
  const run = (fn, resolve, ...args) => {
    active++
    const result = fn(...args)
    resolve(result)
    result.then(next, next)
  }
  const enqueue = (fn, resolve, ...args) => {
    if (active < concurrency) {
      run(fn, resolve, ...args)
    } else {
      queue.push(run.bind(null, fn, resolve, ...args))
    }
  }
  return (fn, ...args) => new Promise(resolve => enqueue(fn, resolve, ...args))
}

export { requestQueue }

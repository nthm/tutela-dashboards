const size = {
  // keep track of how many bytes of JSON have been written
  total: 0,
  format: bytes => `${(bytes / 1e6).toFixed(2)} MB (${bytes} bytes)`,
}

export { size }

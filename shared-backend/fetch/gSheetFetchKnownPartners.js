const fs = require('fs-extra')
const path = require('path')
const fetch = require('node-fetch')
const minimist = require('minimist')

const argv = minimist(process.argv.slice(2))

const baseDir = path.resolve(__dirname, '..', '..', 'data')
fs.ensureDirSync(baseDir)

const gSheetPath = path.join(baseDir, 'gSheetKnownPartners.json')
const gSheetLink = 'https://script.google.com/>.>/exec'

if (argv.overwrite !== true && fs.existsSync(gSheetPath)) {
  console.log('Not overwriting gSheet')
  return
}

console.log('Fetching gSheet')
fetch(`${gSheetLink}?token=>.>&operation=all`)
  .then(res => {
    if (!res.ok) {
      throw new Error(res)
    }
    return res.json()
  })
  .then(data => {
    if ('error' in data || !Array.isArray(data.result)) {
      console.log(data)
      throw new Error('Error in gSheet response')
    }
    fs.writeJSONSync(gSheetPath, data.result, { spaces: 2 })
  })

import fs from 'fs-extra'
import path from 'path'

const baseDir = path.resolve(__dirname, '..', '..', '..', 'data')
const gSheetPath = path.join(baseDir, 'gSheetKnownPartners.json')

if (!fs.existsSync(gSheetPath)) {
  throw new Error('No gSheet. Run gSheetFetch first')
}
const gSheet = fs.readJSONSync(gSheetPath)

const gSheetAPIKeysToParams = () =>
  gSheet.reduce((total, row) => {
    // optional. use 'parameter' for nicer filenames
    total[row['api-key']] = row['partner-name']
    return total
  }, {})

export { gSheetAPIKeysToParams }

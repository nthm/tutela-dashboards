const isoDate = date => date.toISOString().split('T')[0]

const rangeISODates = (start, end) => {
  // handle dates as needed by both Status Dashboard and AppMetrics
  console.log(`Date range: '${start}' until '${end}'`)

  const startDate = new Date(start)
  const stopDate = new Date(end)

  const isoDates = []
  while (startDate <= stopDate) {
    isoDates.push(isoDate(startDate))
    startDate.setDate(startDate.getDate() + 1)
  }
  return isoDates
}

export { rangeISODates }

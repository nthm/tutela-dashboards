// produces:

// mapPackageToPartner.json:
// {
//   "com.myapp.hello": "MyParentCompany",
//   "com.software.kinda": "OtherCompany",
//   "net.app.weather": "GamesCo",
//   ...
// }

const fs = require('fs-extra')
const path = require('path')

const baseDir = path.resolve(__dirname, '..', '..', 'data')
const integrationDir = path.join(baseDir, 'raw', 'deployment-tracking')
const noJSONExt = filename => filename.split('.json')[0]

const gSheetPath = path.join(baseDir, 'gSheetKnownPartners.json')
if (!fs.existsSync(gSheetPath)) {
  throw new Error('No gSheet. Run gSheetFetch first')
}
const gSheet = fs.readJSONSync(gSheetPath)

// this is going to be JSON'd so don't use a Map()
const mapPackageToPartner = {}
const mapPartnerToPackageList = {}

console.log('Building partner JSON mappings')
console.log()

fs.readdirSync(integrationDir)
  .forEach(filename => {
    // undo the `${key.substr(0,6)}-${name.replace(/\s+/g, '_')}` in fetching
    // TODO: this isn't great. conversion isn't always accurate...
    const [keyStart, safeName] = noJSONExt(filename).split('-')
    const name = safeName.replace(/_/g, ' ')

    // api-key
    // parameter
    // partner-name
    // partner-report
    const partner = gSheet.find(x =>
      x['api-key'].startsWith(keyStart) && x['partner-name'] === name)

    const partnerNiceName = partner['partner-name']
    if (!partnerNiceName) {
      // if this EVER happens then increase the keyStart from 6 characters to
      // something with sufficient entropy (maybe just the entire API key) and
      // match that instead of this so-so name matching
      throw new Error('Not able to match a filename to partner nice name')
    }
    const partnerFile = path.join(integrationDir, filename)
    const packages = fs.readJSONSync(partnerFile)
    console.log(`${packages.length} integrations from ${partnerNiceName} (derived from "${filename}")`)
    packages.forEach(({
      // there are a lot of fields here from the gSheet. only take `packageName`
      packageName,
    }) => {
      mapPackageToPartner[packageName] = partnerNiceName

      // this makes an assumption. dropping the API key, so if two partners have
      // the same name then they are the same
      partnerNiceName in mapPartnerToPackageList
        ? mapPartnerToPackageList[partnerNiceName].push(packageName)
        : mapPartnerToPackageList[partnerNiceName] = [packageName]
    })
  })

console.log()
console.log(`${Object.keys(mapPackageToPartner).length} total integrations`)

function writeJSON(path, data) {
  console.log('Writing to', path)
  fs.writeJSONSync(path, data, { spaces: 2 })
}

writeJSON(path.join(baseDir, 'mapPackageToPartner.json'), mapPackageToPartner)
writeJSON(path.join(baseDir, 'mapPartnerToPackageList.json'), mapPartnerToPackageList)

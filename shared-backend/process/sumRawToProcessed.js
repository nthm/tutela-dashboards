// this does some preliminary preprocessing of the raw data into a form that's
// easier to aggregate later for seperate dashboards

const fs = require('fs-extra')
const path = require('path')

// TODO: keep this in sync with the fetch scripts. could make it a config file
const rawDirs = {
  AM: 'name-one',
  DT: 'name-two',
  SD: 'name-three',
}

const noExtJSON = filename => filename.split('.json')[0]
const baseDir = path.resolve(__dirname, '..', '..', 'data')

// convert to paths
Object.keys(rawDirs).forEach(x => {
  rawDirs[x] = path.join(baseDir, 'raw', rawDirs[x])
})

// take the Set() of the ISO-date-based raws
const rawDirPathsISO = [rawDirs.AM, rawDirs.SD]
const rawISOFiles =
  Array.from(new Set(...rawDirPathsISO.map(x => fs.readdirSync(x))))

const processedDir = path.join(baseDir, 'processed')
fs.ensureDirSync(processedDir)

const processedFiles = fs.readdirSync(processedDir)
const queue = rawISOFiles.filter(file => !processedFiles.includes(file))

console.log('Missing files to be processed:')
console.log(queue)

// load all of these first since there's only so many of them - about 8190
const crowdNameToPackageName = {} // new Map()
fs.readdirSync(rawDirs.DT).forEach(filename => {
  // eslint-disable-next-line global-require,import/no-dynamic-require
  const data = require(path.join(rawDirs.DT, filename))
  data.forEach(({
    // lots of fields that I'll delete for privacy reasons...
    name,
    packageName,
  }) => {
    if (name in crowdNameToPackageName) {
      console.log(`${name} already exists in name to package name mapping`)
    }
    crowdNameToPackageName[name] = packageName
  })
})

const noNiceNames = new Set()
const packageName = name => {
  let niceName = crowdNameToPackageName[name]
  // it's OK for many deployment names to match to one package name
  if (!niceName) {
    noNiceNames.add(name)
    niceName = name
  }
  return niceName
}

// assume a file of this name exists in both remotes
queue.forEach(ISODateFilename => {
  const date = noExtJSON(ISODateFilename)
  const result = {
    Date: date,
    // for global-status. also a sanity check - should match data from dataflow
    TotalGlobal: {
      MetricA: 0,
      MetricB: 0,
    },
    GloballySD: {
      // <Integration>: { MetricA, MetricB },
    },
    PerCountryAM: {
      // <Integration>: { <Country>: { MetricA, MetricB } },
    },
  }
  const logSD = `${date} Status Dashboard`
  console.time(logSD)
  // eslint-disable-next-line global-require,import/no-dynamic-require
  const dataSD = require(path.join(rawDirs.SD, ISODateFilename))
  console.timeEnd(logSD)

  dataSD.forEach(({
    DeploymentName, MetricA, MetricB,
  }) => {
    result.TotalGlobal.MetricA += MetricA
    result.TotalGlobal.MetricB += MetricB

    const niceName = packageName(DeploymentName)
    if (!(niceName in result.GloballySD)) {
      result.GloballySD[niceName] = {
        MetricA: 0,
        MetricB: 0,
      }
    }
    result.GloballySD[niceName].MetricA += MetricA
    result.GloballySD[niceName].MetricB += MetricB
  })

  const logAM = `${date} AppMetrics`
  console.time(logAM)
  // eslint-disable-next-line global-require,import/no-dynamic-require
  const dataAM = require(path.join(rawDirs.AM, ISODateFilename))
  console.timeEnd(logAM)

  dataAM.forEach(({
    Name, Country, MetricA, MetricB,
  }) => {
    const niceName = packageName(Name)
    if (!(niceName in result.PerCountryAM)) {
      result.PerCountryAM[niceName] = {}
    }
    if (!(Country in result.PerCountryAM[niceName])) {
      result.PerCountryAM[niceName][Country] = {
        MetricA: 0,
        MetricB: 0,
      }
    }
    result.PerCountryAM[niceName][Country].MetricA += MetricA
    result.PerCountryAM[niceName][Country].MetricB += MetricB
  })

  fs.writeJSON(path.join(processedDir, ISODateFilename), result, { spaces: 2 })
    .catch(err => {
      console.log('Failed to write', date)
      console.log(err)
    })
})
console.log(`Warn: ${noNiceNames.size} integrations didn't match a nice name`)
console.log(noNiceNames.keys())

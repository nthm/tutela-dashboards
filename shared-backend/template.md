Here's the common code/template used for each dashboard. They all work by taking
data from build/data/processed/ and then furthur aggregating it into a report.

The processed data is sorted by date, so that needs to be looped, and accept
arguments to specify what date range you want. Since it's common, but had to
factor out into a module, here's the code.

In the template here, the loop stops at either the end date range given, or the
end of files in processed/ - which ever is first.

```js
const fs = require('fs-extra')
const path = require('path')
const minimist = require('minimist')

const argv = minimist(process.argv.slice(2))

const isoDate = date => date.toISOString().split('T')[0]
const validDate = date => isNaN(Date.parse(date)) === false
const noJSONExt = filename => filename.split('.json')[0]

const processedDir = path.resolve(__dirname, '../build/data/processed')
// can easily sort ISO dates
const processedFiles = fs.readdirSync(processedDir).sort()

// fallback values if not told a date range to process
let startDate = noJSONExt(processedFiles[0])
let endDate = noJSONExt(processedFiles[processedFiles.length - 1])

let startThreshold = -Infinity
if (('from' in argv) && validDate(argv.from)) {
  console.log('ISO date --from given. Limiting data range')
  const date = new Date(argv.from)
  startThreshold = date.getTime()
  startDate = isoDate(date)
}
let endThreshold = Infinity
if (('to' in argv) && validDate(argv.to)) {
  console.log('ISO date --to given. Limiting data range')
  const date = new Date(argv.to)
  endThreshold = date.getTime()
  endDate = isoDate(date)
}

//
// TODO: setup variables here to keep track of everything (counters, lists, etc)
//

let previousDate = null
for (const filename of processedFiles) {
  const fileISO = noJSONExt(filename)
  const currentDate = new Date(fileISO)
  const currentTimestamp = currentDate.getTime()

  if (previousDate !== null) {
    const isoPrevious = isoDate(previousDate)
    // move `previous` up. hopefully to `current`, but maybe less
    previousDate.setDate(previousDate.getDate() + 1)
    if (isoDate(previousDate) !== fileISO) {
      console.log(`Warning: Data gap between ${isoPrevious} and ${fileISO}`)
    }
  } else {
    console.log('Starting at', fileISO)
  }
  // move `previous` up to `current`
  previousDate = currentDate
  if (currentTimestamp < startThreshold) {
    // gaps in data are still reported, but otherwise there's silence
    continue
  }
  if (currentTimestamp > endThreshold) {
    console.log(filename, 'is beyond is the end date. Not processing. Exiting.')
    break
  }

  // load in the data file
  console.log(fileISO)
  // eslint-disable-next-line global-require,import/no-dynamic-require
  const data = require(path.join(processedDir, filename))

  //
  // TODO: your aggregations here. nested for-loops or whathaveyou.
  //
}
```

Another way of doing it is to assume defaults when values are not given for the
arguments `--from`/`--to`. Note that in the code below, the values of `argv` are
overwritten to simulate input.

```js
// determine which dates are wanted by both Status Dashboard and AppMetrics
// they're compared to the existing files
const now = new Date()
const isoDate = date => date.toISOString().split('T')[0]

now.setDate(now.getDate() - 31)
const thirtyDaysFromYesterday = isoDate(now)
// don't use !('to' in argv) and minimist will turn `--to` into `{ to: true }`
if (!argv.from || argv.from === true) {
  console.log(`'--from' not provided. Setting to '${thirtyDaysFromYesterday}'`)
  argv.from = thirtyDaysFromYesterday
}

now.setDate(now.getDate() + 30)
const yesterday = isoDate(now)
if (!argv.to || argv.to === true) {
  console.log(`'--to' not provided. Setting to '${yesterday}'`)
  argv.to = yesterday
}
```


Lastly, it might be useful to know how to take intersections of sets of files.
One of the processing scripts performs a three-way comparison to minimize the
amount of work done on each run.

It's common to need to pull from multiple data sources, particularly in fetching
data (which, hopefully you'll never need to do because data/processed/ should be
enough for you). If you're pulling from multiple places for processing, here's
a simple way to compare sets fo files:

```
data/
  raw/
    name-one/
    name-two/
    name-three/
  processed/
```

```js
const baseDir = path.resolve(__dirname, '..', '..', 'build', 'data')

// these are the names of the folders for each of the data providers
const rawDirs = {
  AM: 'name-one',
  DT: 'name-two',
  SD: 'name-three',
}

// convert each of them to paths
Object.keys(rawDirs).forEach(x => {
  rawDirs[x] = path.join(baseDir, 'raw', rawDirs[x])
})

// take the Set() of the ISO-date-based raws
const rawDirPathsISO = [rawDirs.AM, rawDirs.SD]
const rawISOFiles =
  Array.from(new Set(...rawDirPathsISO.map(x => fs.readdirSync(x))))

const processedDir = path.join(baseDir, 'processed')
fs.ensureDirSync(processedDir)

const processedFiles = fs.readdirSync(processedDir)
const queue = rawISOFiles.filter(file => !processedFiles.includes(file))

console.log('Missing files to be processed:')
console.log(queue)
```

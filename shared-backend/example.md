Example run:

Note this is designed to do minimum work. So if you already have files it won't
download them.

```
> npm run data:download -- --from 2018-06-20

> dashboards@0.0.0 data:download ./dashboard-country-status
> node shared-backend/fetch/gSheetFetchKnownPartners && node shared-backend/fetch "--from" "2018-06-20"

Not overwriting gSheet
'--to' not provided. Setting to '2018-08-22'
Date range: '2018-06-20' until '2018-08-22'w

Looking for pre-processed files in S3
Assumed role: { ResponseMetadata: { RequestId: '' },
  Credentials:
   { AccessKeyId: '>.>',
     SecretAccessKey: '>.>',
     SessionToken: '',
     Expiration: },
  AssumedRoleUser:
   { AssumedRoleId: '',
     Arn: 'arn:aws:sts::' } }
S3 has 0 processed files

Looking for files in these remote servers:

  - S3 Processed:
    ./dashboard-country-status/data/processed


  - Status Dashboard API Server:
    ./dashboard-country-status/data/raw/status-dashboard
    - 2018-06-20
    - 2018-06-21
    - 2018-06-22
    - 2018-06-23
    - 2018-06-24
    - 2018-06-25
    - 2018-06-26
    - 2018-06-27
    - 2018-06-28
    - 2018-06-29
    - 2018-06-30
    - 2018-07-01
    - 2018-07-02
    - 2018-07-03
    - 2018-07-04
    - 2018-07-05
    - 2018-07-06
    - 2018-07-07
    - 2018-07-08
    - 2018-07-09
    - 2018-07-10
    - 2018-07-11
    - 2018-07-12
    - 2018-07-13
    - 2018-07-14
    - 2018-07-15
    - 2018-07-19
    - 2018-07-20
    - 2018-07-21
    - 2018-07-22


  - Deployment Tracking Server:
    ./dashboard-country-status/data/raw/deployment-tracking


  - Local API Server:
    ./dashboard-country-status/data/raw/local-server
    - 2018-06-20
    - 2018-06-21
    - 2018-06-22
    - 2018-06-23
    - 2018-06-24
    - 2018-06-25
    - 2018-06-26
    - 2018-06-27
    - 2018-06-28
    - 2018-06-29
    - 2018-06-30
    - 2018-07-01
    - 2018-07-02
    - 2018-07-03
    - 2018-07-04
    - 2018-07-05
    - 2018-07-06
    - 2018-07-07
    - 2018-07-08
    - 2018-07-09
    - 2018-07-10
    - 2018-07-11
    - 2018-07-12
    - 2018-07-13
    - 2018-07-14
    - 2018-07-15
    - 2018-07-19
    - 2018-07-20
    - 2018-07-21
    - 2018-07-22

Downloading in parallel.

Started: Status Dashboard API Server 2018-06-20
Started: Status Dashboard API Server 2018-06-21
Started: Status Dashboard API Server 2018-06-22
Started: Status Dashboard API Server 2018-06-23
Started: Status Dashboard API Server 2018-06-24
Started: Status Dashboard API Server 2018-06-25
Started: Status Dashboard API Server 2018-06-26
Started: Status Dashboard API Server 2018-06-27
Started: Status Dashboard API Server 2018-06-28
Started: Status Dashboard API Server 2018-06-29
Started: Status Dashboard API Server 2018-06-30
Started: Status Dashboard API Server 2018-07-01
Started: Status Dashboard API Server 2018-07-02
Started: Status Dashboard API Server 2018-07-03
Started: Status Dashboard API Server 2018-07-04
Started: Local API Server 2018-06-20
Started: Local API Server 2018-06-21
Started: Local API Server 2018-06-22
Started: Local API Server 2018-06-23
Started: Local API Server 2018-06-24
Done: Status Dashboard 2018-06-22: 422.594ms
JSON size: 0.29 MB (290994 bytes)
Started: Status Dashboard 2018-07-05
Done: Status Dashboard 2018-06-23: 427.897ms
JSON size: 0.29 MB (289051 bytes)
...
Done: Local API Server 2018-07-13: 3051.679ms
JSON size: 1.88 MB (1882422 bytes)
Started: Local API Server 2018-07-21
Done: Local API Server 2018-07-14: 3598.480ms
JSON size: 1.86 MB (1862970 bytes)
Started: Local API Server 2018-07-22
Done: Local API Server 2018-07-15: 3608.464ms
JSON size: 1.88 MB (1882117 bytes)
Done: Local API Server 2018-07-20: 3316.321ms
JSON size: 1.93 MB (1928812 bytes)
Done: Local API Server 2018-07-19: 4400.662ms
JSON size: 1.92 MB (1920524 bytes)
Done: Local API Server 2018-07-21: 3915.752ms
JSON size: 1.92 MB (1921515 bytes)
Done: Local API Server 2018-07-22: 3422.166ms
JSON size: 1.93 MB (1930359 bytes)
Total JSON written: 65.12 MB (65121496 bytes)
```

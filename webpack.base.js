const webpack = require('webpack')
const path = require('path')

const UglifyJSPlugin = require('uglifyjs-webpack-plugin') // soon built-in...
const MiniCSSExtractPlugin = require('mini-css-extract-plugin')
const SpritePlugin = require('svg-sprite-loader/plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const HtmlPlugin = require('html-webpack-plugin')

const babelConfig = {
  // cache builds in node_modules/.cache/babel-loader/
  cacheDirectory: true,
  presets: [
    [
      '@babel/preset-env', {
        modules: false,
        useBuiltIns: 'entry',
        targets: { browsers: [ 'last 2 Chrome versions' ] },
      },
    ],
    '@babel/preset-react',
  ],
  plugins: [
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-syntax-dynamic-import',
  ],
}

function makeBaseConfig({ mode, tree, htmlTitle }) {
  const nodeEnv = mode || process.env.NODE_ENV || 'production'
  const config = {
    entry: path.join(tree, 'index.js'),
    output: {
      // don't specify a path here. that's not a `base` thing
      filename: 'bundle.js',
    },
    externals: {
      // syntax is `module name: global namespace (window.xyz)`
      'react': 'React',
      'react-dom': 'ReactDOM',
    },
    plugins: [
      new HtmlPlugin({
        title: htmlTitle,
        template: path.join(tree, 'index.ejs'),
        minify: {
          minifyCSS: true,
          minifyJS: true,
          removeComments: true,
          collapseWhitespace: true,
          preserveLineBreaks: false,
          removeScriptTypeAttributes: true,
          removeRedundantAttributes: true,
          removeStyleLinkTypeAttributes: true,
        },
      }),
      new MiniCSSExtractPlugin({ filename: 'style.css' }),
      new SpritePlugin({
        plainSprite: true,
      }),
    ],
    mode: nodeEnv,
    module: {
      // order matters for: rules, use, presets, plugins
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          enforce: 'pre', // important
          use: { loader: 'eslint-loader', options: { emitError: true } },
        }, {
          test: /\.js$/,
          exclude: /node_modules/,
          use: { loader: 'babel-loader', options: babelConfig },
        }, {
          test: /\.css$/,
          use: [MiniCSSExtractPlugin.loader, 'css-loader'],
        }, {
          test: /\.svg$/,
          // see github svg-sprite-loader/issues/123
          use: { loader: 'svg-sprite-loader', options: { extract: false } },
        }, {
          test: /\.woff2$/,
          use: 'file-loader?name=/[name].woff2',
        },
      ],
    },
    performance: {
      // a minimum React app is ~500kb (~200kb gzip). warn at 1mb
      maxEntrypointSize: 1000000,
      maxAssetSize: 1000000,
    },
  }
  if (nodeEnv === 'development') {
    // add /* filename */ comments to generated require()s
    config.output.pathinfo = true
    config.optimization.namedModules = true
    config.plugins.push(
      new webpack.HotModuleReplacementPlugin())
    config.devServer = {
      hot: true,
      compress: true,
      port: '8080',
      host: '0.0.0.0',
      disableHostCheck: true,
    }
  }

  if (nodeEnv === 'production') {
    config.plugins.push(
      new BundleAnalyzerPlugin({
        // avoid confusion with the reports/ directory right next to it
        reportFilename: 'analyzer.html',
        analyzerMode: 'static',
        openAnalyzer: false,
        generateStatsFile: false,
      }))
    config.optimization = {
      minimizer: [
        new UglifyJSPlugin({
          cache: true,
          parallel: true,
          sourceMap: false, // set to true if you want JS source maps
        }),
        new OptimizeCSSAssetsPlugin({}),
      ],
    }
  }

  return config
}

module.exports = makeBaseConfig

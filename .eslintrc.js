module.exports = {
  'extends': [
    'eslint:recommended',
    'plugin:react/recommended',
  ],
  'settings': {
    'react': {
      'version': '16.4.2',
    },
  },
  'plugins': [
    'babel',
  ],
  'env': {
    'browser': true,
    'node': true,
    // https://github.com/eslint/eslint/issues/9812#issuecomment-355772014
    'es6': true,
  },
  'parser': 'babel-eslint',
  'parserOptions': {
    'ecmaVersion': 8,
    'sourceType': 'module',
    'ecmaFeatures': {
      'experimentalObjectRestSpread': true,
      'jsx': true,
    },
  },
  'rules': {
    'indent': ['error', 2, {
      'SwitchCase': 1,
    }],
    'eol-last': ['error', 'always'],
    'babel/semi': ['error', 'never'],
    'babel/quotes': ['error', 'single'],
    'jsx-quotes': ['error', 'prefer-single'],
    'comma-dangle': ['error', {
      'arrays': 'always-multiline',
      'objects': 'always-multiline',
      'imports': 'never',
      'exports': 'never',
      'functions': 'never',
    }],
    'space-before-function-paren': ['error', {
      'anonymous': 'always',
      'named': 'never',
      'asyncArrow': 'always',
    }],
    'no-console': 'off',
    'no-multi-spaces': ['error', {
      'ignoreEOLComments': true,
    }],
    'object-curly-spacing': ['error', 'always'],
    'react/no-unescaped-entities': ['error', {
      'forbid': ['>', '}'],
    }],
    'react/display-name': 'off',
  },
}

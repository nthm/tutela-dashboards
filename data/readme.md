Most files in this folder will be generated or downloaded from scripts in
shared-backend/. Data either comes from S3, a Tutela dataservice, or a gSheet

Only JSON that cannot be generated is commited here, such as `countryCodes`.

See rules in .gitignore

// this meant to be used in Jenkins so it's super simple for a `sh()` call

const fetch = require('node-fetch')

const gSheetLink = 'https://script.google.com/a/>.>/exec'

fetch(`${gSheetLink}?token=>.>&operation=>.>`)
  .then(res => {
    if (!res.ok) {
      // this will return a bad status code
      throw new Error(res)
    }
    return res.json()
  })
  .then(data => {
    if ('error' in data || !Array.isArray(data.result)) {
      console.log(data)
      throw new Error('Unexpected gSheet response')
    }
    // the "return" to sh()
    console.log(data.result.join(',').trim())
  })

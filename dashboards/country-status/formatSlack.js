// load res/data/sidebar.json and process into a nice Slack message. this is
// called once per country and appended to a file (Jenkins) so it can't have
// headers and nice things. that's in the Jenkinsfile

const path = require('path')
const minimist = require('minimist')
const cc = require('../../data/countryCodes.json')

const argv = minimist(process.argv.slice(2))

if (!('country' in argv)) {
  throw new Error('Must provide a country')
}

const data = path.resolve(__dirname, 'build', 'aggregated-data',
  argv.country, 'sidebar.json')
const { country, stats } = require(data)

// don't bother if device count is too small
if (stats[0].value < 10000) {
  return
}

const emoji = percent => {
  if (percent <= -50)
    return ':negative_squared_cross_mark:'
  if (percent <= -10)
    return ':arrow_down:'
  if (percent <= -2)
    return ':arrow_lower_right:'

  if (percent >= 50)
    return ':partyblob:'
  if (percent >= 10)
    return ':arrow_double_up:'
  if (percent >= 2)
    return ':arrow_up_small:'

  // else, between -2 and 2 - this should never be displayed due to threshold
  return ':ok:'
}

// the order of stats is "Devices yesterday", "Tests yesterday", "Tests 30 days"
// which is ignored, and "Live app integrations"
const text = ['Devices', 'Tests', 'Apps']
const changes = [0, 1, 3].map(x => stats[x].change)
const messageParts = []
const withSign = x => x < 0 ? `${x}` : `+${x}`

changes.forEach((percent, index) => {
  // threshold for whether it's worth displaying to Slack
  if (Math.abs(percent) > 2) {
    messageParts.push(`${text[index]}: ${withSign(percent)}% ${emoji(percent)}`)
  }
})

if (messageParts.length === 0) {
  return
}

const code = country in cc && cc[country].toLowerCase()
const flag = code
  ? `:flag-${code}:`
  : ':question:'

console.log(`${flag} *${country}*: ${messageParts.join(', ')}`)

// processedToDashboard

// given a country, aggregate data from processed/ to values for that country.
// building the frontend dashboard compiles a general bundle and HTML document
// that has no data. this adds data to that HTML

// after aggregation, it reads the minified build/index.html created by
// HTMLWebpackPlugin and creates build/reports/[country].html

// useful for Jenkins where the pipeline builds one bundle, once, and then
// generates different set of data JSONs for 77 countries

const fs = require('fs-extra')
const path = require('path')
const minimist = require('minimist')

const buildDir = path.resolve(__dirname, 'build')
const dataDir = path.resolve(__dirname, '..', '..', 'data')
const processedDir = path.join(dataDir, 'processed')

// semi-external require() sorry
const integrationToAppName = require(path.join(dataDir,
  'mapPackageToPartner.json'))

const argv = minimist(process.argv.slice(2))

if (!('country' in argv)) {
  throw new Error('Must provide a country')
}

if (!fs.existsSync(path.join(buildDir, 'index.html'))) {
  throw new Error('No build/index.html. Create it with `npm run build` first.')
}

const selectedCountry = argv.country

const isoDate = date => date.toISOString().split('T')[0]
const validDate = date => Number.isNaN(Date.parse(date)) === false
const noJSONExt = filename => filename.split('.json')[0]
const niceBytes = bytes => `${(bytes / 1e6).toFixed(2)} MB (${bytes} bytes)`

// can easily sort ISO dates
const processedFiles = fs.readdirSync(processedDir).sort()

// fallback values if not told a date range to process
let startDate = noJSONExt(processedFiles[0])
let endDate = noJSONExt(processedFiles[processedFiles.length - 1])

// don't assume and set yesterday/thirtyDaysFromYesterday like shared-backend
let startThreshold = -Infinity
if (('from' in argv) && validDate(argv.from)) {
  console.log('ISO date --from given. Limiting data range')
  const date = new Date(argv.from)
  startThreshold = date.getTime()
  startDate = isoDate(date)
}

let lastDay
let endThreshold = Infinity
if (('to' in argv) && validDate(argv.to)) {
  console.log('ISO date --to given. Limiting data range')
  lastDay = argv.to
  const date = new Date(argv.to)
  endThreshold = date.getTime()
  endDate = isoDate(date)
}

// fallback to last file
if (!lastDay) {
  lastDay = noJSONExt(processedFiles[processedFiles.length - 1])
}

console.log(lastDay)

const integrations = new Set()
const integrationSums = new Map()
const ISODateToIntegrationToCounts = new Map()

// for ReCharts
const topChart = []
const bottomChart = []

// used by integration counts
const dayBeforeEndDate = (() => {
  const date = new Date(endDate)
  date.setDate(date.getDate() - 1)
  return isoDate(date)
})()

let integrationCountLastDay = null
let integrationCountSecondLastDay = null

let previousDate = null
for (const filename of processedFiles) {
  const fileISO = noJSONExt(filename)
  const currentDate = new Date(fileISO)
  const currentTimestamp = currentDate.getTime()

  if (previousDate !== null) {
    const isoPrevious = isoDate(previousDate)
    // move `previous` up. hopefully to `current`, but maybe less
    previousDate.setDate(previousDate.getDate() + 1)
    if (isoDate(previousDate) !== fileISO) {
      console.log(`Warning: Data gap between ${isoPrevious} and ${fileISO}`)
    }
  } else {
    console.log('Starting at', fileISO)
  }
  // move `previous` up to `current`
  previousDate = currentDate
  if (currentTimestamp < startThreshold) {
    // gaps in data are still reported, but otherwise there's silence
    continue
  }
  if (currentTimestamp > endThreshold) {
    console.log(filename, 'is beyond is the end date. Not processing. Exiting.')
    break
  }
  const dataPath = path.join(processedDir, filename)
  const { size } = fs.statSync(dataPath)
  console.log(fileISO)
  console.log(`Data file is ${niceBytes(size)}`)

  // eslint-disable-next-line global-require,import/no-dynamic-require
  const data = require(path.join(processedDir, filename))

  const accumulationForCountry = {
    Count: 0,
    TestCount: 0,
  }

  // handle integrations
  Object.entries(data.PerCountryAM).forEach(([integrationName, perCountry]) => {
    for (const country in perCountry) {
      if (country !== selectedCountry) {
        continue
      }
      // try converting. might not be possible
      const partner = integrationToAppName[integrationName]
      const appName = integrationName + (partner ? ` (${partner})` : '')
      integrations.add(appName)

      // only handle count for now
      const { Count, TestCount } = perCountry[country]

      accumulationForCountry.Count += Count
      accumulationForCountry.TestCount += TestCount

      let current

      // upserts
      current = integrationSums.get(appName) || 0
      integrationSums.set(appName, current + Count)

      current = ISODateToIntegrationToCounts.get(fileISO) || { [appName]: 0 }
      if (appName in current) {
        current[appName] += Count
      } else {
        current[appName] = Count
      }
      ISODateToIntegrationToCounts.set(fileISO, current)
    }
  })

  topChart.push({
    Date: fileISO,
    Count: accumulationForCountry.Count,
    TestCount: accumulationForCountry.TestCount,
  })

  // possibly handle integration counts
  if (fileISO === dayBeforeEndDate) {
    integrationCountSecondLastDay =
      Object.values(data.GloballySD).filter(obj => obj.Count > 0).length
  }
  if (fileISO === endDate) {
    integrationCountLastDay =
      Object.values(data.GloballySD).filter(obj => obj.Count > 0).length
  }
}

const days = ISODateToIntegrationToCounts.size
console.log()
console.log(`Processed ${days} days`)
console.log(`Accumulated ${integrationSums.size} integrations`)
console.log()

const allFinalIntegrations = [...integrations.values()]

// decide which to keep by sorting their totals
const integrationTotals = {}

ISODateToIntegrationToCounts.forEach((integrationToCounts, date) => {
  Object.entries(integrationToCounts).forEach(([packageName, Count]) => {
    if (!(packageName in integrationTotals)) {
      integrationTotals[packageName] = 0
    }
    integrationTotals[packageName] += Count
  })
  const resultCount = { Date: date }

  // need every single row of the data for ReCharts to have every integration
  // so, yes, there's a lot of zeros
  allFinalIntegrations.forEach(inte => {
    resultCount[inte] = integrationToCounts[inte] || 0
  })
  bottomChart.push(resultCount)
})

// ok now we have this massive unrenderable JSON of thousands of packages.
// sort it. reduce to 10

const topPackages =
  Object.keys(integrationTotals)
    .sort((a, b) =>
      integrationTotals[b] - integrationTotals[a])
    .slice(0, 10)

const reducedBottomChart = bottomChart.map(obj =>
  topPackages.reduce((acc, name) => {
    // FIXME:
    acc[name] = obj[name] < 1000 ? 1000 : obj[name]
    return acc
  }, { Date: obj.Date }))

// calculate sidebar values

const percentChange = (day, dayBefore) =>
  (((day - dayBefore) / dayBefore) * 100).toFixed(1)

// popping (see below) is destructive so do this reduction first:
const testsPerformed30Days =
  topChart.reduce((sum, { TestCount }) => sum + TestCount, 0)

const globalLastDay = topChart[topChart.length - 1]
const globalSecondLastDay = topChart[topChart.length - 2]

const sidebar = {
  dateRange: `${startDate} to ${endDate}`,
  generatedOn: (new Date()).toUTCString(),
  country: selectedCountry,
  stats: [
    // for: Title A
    {
      'value': globalLastDay.Count,
      'change': percentChange(
        globalLastDay.Count,
        globalSecondLastDay.Count
      ),
    },
    // for: Title B
    {
      'value': globalLastDay.TestCount,
      'change': percentChange(
        globalLastDay.TestCount,
        globalSecondLastDay.TestCount
      ),
    },
    // for: Title C
    {
      'value': testsPerformed30Days,
    },
    // for: Title D
    {
      'value': integrationCountLastDay,
      'change': percentChange(
        integrationCountLastDay,
        integrationCountSecondLastDay
      ),
    },
  ],
}

// TODO: this is likely very expensive since it's introducing a string buffer of
// the entire JSON (rather large). ideally use fs.read and fs.write to write the
// JSON into the file without reading all of it in

console.log('Merging data into empty dashboard template')
const html = fs.readFileSync(path.join(buildDir, 'index.html'), 'utf8')

const newHtml = html
  .replace(
    'metricCountA:null',
    'metricCountA:' + JSON.stringify(topChart)
  )
  .replace(
    'metricCountPerColumn:null',
    'metricCountPerColumn:' + JSON.stringify(reducedBottomChart)
  )
  .replace(
    'sidebar:null',
    'sidebar:' + JSON.stringify(sidebar)
  )

// sanity check
if (newHtml === html) {
  throw new Error('Nothing changed while trying to merge data, bailing')
}

const reportsDir = path.join(buildDir, 'reports')
fs.ensureDirSync(reportsDir)
fs.writeFileSync(path.join(reportsDir, selectedCountry + '.html'), newHtml)
console.log(`Saved to /build/reports/${selectedCountry}.html`)

// this is used for debugging, and by formatSidebarForSlack

const outDir = path.join(buildDir, 'aggregated-data', selectedCountry)
fs.ensureDirSync(outDir)

const writeJSON = (name, data) =>
  fs.writeJSONSync(path.join(outDir, name), data, { spaces: 2 })

writeJSON('metricCountA.json', topChart)
writeJSON('metricCountPerColumn.json', reducedBottomChart)
writeJSON('sidebar.json', sidebar)

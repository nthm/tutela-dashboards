const fs = require('fs-extra')
const path = require('path')
const minimist = require('minimist')

const buildDir = path.resolve(__dirname, 'build')
const dataDir = path.resolve(__dirname, '..', '..', 'data')
const processedDir = path.join(dataDir, 'processed')

if (!fs.existsSync(path.join(buildDir, 'index.html'))) {
  throw new Error('No build/index.html. Create it with `npm run build` first.')
}

const argv = minimist(process.argv.slice(2))

if (!('partner' in argv)) {
  throw new Error('Must provide a partner parameter')
}

// this will be the nice name. no more conversion back and forth
// now just type "4Shared" not "forShared"
const { partner } = argv

const listFile = path.join(dataDir, 'mapPartnerToPackageList.json')
const packageNames = fs.readJSONSync(listFile)[partner]

const isoDate = date => date.toISOString().split('T')[0]
const validDate = date => Number.isNaN(Date.parse(date)) === false
const noJSONExt = filename => filename.split('.json')[0]
const niceBytes = bytes => `${(bytes / 1e6).toFixed(2)} MB (${bytes} bytes)`

// can easily sort ISO dates
const processedFiles = fs.readdirSync(processedDir).sort()

// be less forgiving than run.js
let startThreshold = -Infinity
if (('from' in argv) && validDate(argv.from)) {
  console.log('ISO date --from given. Limiting data range')
  const date = new Date(argv.from)
  startThreshold = date.getTime()
}

let lastDay
let endThreshold = Infinity
if (('to' in argv) && validDate(argv.to)) {
  console.log('ISO date --to given. Limiting data range')
  lastDay = argv.to
  const date = new Date(argv.to)
  endThreshold = date.getTime()
}

// fallback to last file
if (!lastDay) {
  lastDay = noJSONExt(processedFiles[processedFiles.length - 1])
}

console.log(lastDay)

const integrations = new Set()
const ISODateToIntegrationToCounts = new Map()

// don't use a Map since it needs to be JSON'd and that's work
const countryCounts = {}

// for ReCharts
const CountDataPoints = []
const TestCountDataPoints = []

let previousDate = null
for (const filename of processedFiles) {
  const fileISO = noJSONExt(filename)
  const currentDate = new Date(fileISO)
  const currentTimestamp = currentDate.getTime()

  if (previousDate !== null) {
    const isoPrevious = isoDate(previousDate)
    // move `previous` up. hopefully to `current`, but maybe less
    previousDate.setDate(previousDate.getDate() + 1)
    if (isoDate(previousDate) !== fileISO) {
      console.log(`Warning: Data gap between ${isoPrevious} and ${fileISO}`)
    }
  } else {
    console.log('Starting at', fileISO)
  }
  // move `previous` up to `current`
  previousDate = currentDate
  if (currentTimestamp < startThreshold) {
    // gaps in data are still reported, but otherwise there's silence
    continue
  }
  if (currentTimestamp > endThreshold) {
    console.log(filename, 'is beyond is the end date. Not processing. Exiting.')
    break
  }
  const dataPath = path.join(processedDir, filename)
  const { size } = fs.statSync(dataPath)
  console.log(fileISO)
  console.log(`Data file is ${niceBytes(size)}`)
  // eslint-disable-next-line global-require,import/no-dynamic-require
  const data = require(dataPath)

  // handle integrations
  let integrationsThatDay = 0
  for (const integration in data.GloballySD) {
    if (!packageNames.includes(integration)) {
      continue
    }
    integrations.add(integration)
    integrationsThatDay++

    const { Count, TestCount } = data.GloballySD[integration]

    const dateEntry = ISODateToIntegrationToCounts.get(fileISO) || {}
    // insert
    if (!(integration in dateEntry)) {
      dateEntry[integration] = { Count, TestCount }
    } else {
      dateEntry[integration].Count += Count
      dateEntry[integration].TestCount += TestCount
    }
    ISODateToIntegrationToCounts.set(fileISO, dateEntry)
  }

  const allInts = Object.keys(data.GloballySD)
  console.log(`Day had ${allInts.length} integrations. ${integrationsThatDay ||
    'NONE'} were ${partner}\n`)

  for (const integration in data.PerCountryAM) {
    if (!packageNames.includes(integration)) {
      continue
    }
    Object.entries(data.PerCountryAM[integration])
      .forEach(([country, { Count, TestCount }]) => {
        if (!(country in countryCounts)) {
          countryCounts[country] = { Count, TestCount }
        } else {
          countryCounts[country].Count += Count
          countryCounts[country].TestCount += TestCount
        }
        if (fileISO === lastDay) {
          // save this specifically for a column in the report table
          if (!('LastCount' in countryCounts[country])) {
            countryCounts[country].LastCount = Count
          } else {
            countryCounts[country].LastCount += Count
          }
        }
      })
  }
}

const days = ISODateToIntegrationToCounts.size
console.log()
console.log(`Processed ${days} days`)
console.log('Countries:', Object.keys(countryCounts).length)
console.log(`Integrations for ${partner}:`, integrations.size)
console.log()

for (const value of Object.values(countryCounts)) {
  value.AverageCount = value.Count / days
}

const allFinalIntegrations = [...integrations.values()]

// decide which to keep by sorting their totals
const integrationTotals = {}

ISODateToIntegrationToCounts.forEach((integrationToCounts, date) => {
  Object.entries(integrationToCounts).forEach(([packageName, counts]) => {
    if (!(packageName in integrationTotals)) {
      integrationTotals[packageName] = { Count: 0, TestCount: 0 }
    }
    integrationTotals[packageName].Count += counts.Count
    integrationTotals[packageName].TestCount += counts.TestCount
  })
  const resultCount = { Date: date }
  const resultTestCount = { Date: date }

  // need every single row of the data for ReCharts to have every integration
  // so, yes, there's a lot of zeros
  allFinalIntegrations.forEach(inte => {
    const obj = integrationToCounts[inte] || { Count: 0, TestCount: 0 }
    resultCount[inte] = obj.Count
    resultTestCount[inte] = obj.TestCount
  })
  CountDataPoints.push(resultCount)
  TestCountDataPoints.push(resultTestCount)
})

// ok now we have this massive unrenderable JSON of thousands of packages.
// sort it. reduce to 6
const topPackagesbyCount =
  Object.keys(integrationTotals)
    .sort((a, b) =>
      integrationTotals[b].Count - integrationTotals[a].Count)
    .slice(0, 6)

const reducedCountDataPoints = CountDataPoints.map(obj =>
  topPackagesbyCount.reduce((acc, name) => {
    acc[name] = obj[name]
    return acc
  }, { Date: obj.Date }))

const topPackagesbyTestCount =
  Object.keys(integrationTotals)
    .sort((a, b) =>
      integrationTotals[b].TestCount - integrationTotals[a].TestCount)
    .slice(0, 6)

const reducedTestCountDataPoints = TestCountDataPoints.map(obj =>
  topPackagesbyTestCount.reduce((acc, name) => {
    acc[name] = obj[name]
    return acc
  }, { Date: obj.Date }))

const infoData = {
  generatedOn: (new Date()).toUTCString(),
  partnerName: partner,
  days,
}

console.log('Merging data into empty dashboard template')
const html = fs.readFileSync(path.join(buildDir, 'index.html'), 'utf8')

const newHtml = html
  .replace(
    'byCount:null',
    'byCount:' + JSON.stringify(reducedCountDataPoints)
  )
  .replace(
    'byTestCount:null',
    'byTestCount:' + JSON.stringify(reducedTestCountDataPoints)
  )
  .replace(
    'countryCounts:null',
    'countryCounts:' + JSON.stringify(countryCounts)
  )
  .replace(
    'info:null',
    'info:' + JSON.stringify(infoData)
  )

// sanity check
if (newHtml === html) {
  throw new Error('Nothing changed while trying to merge data, bailing')
}

const reportsDir = path.join(buildDir, 'reports')
fs.ensureDirSync(reportsDir)
fs.writeFileSync(path.join(reportsDir, partner + '.html'), newHtml)
console.log(`Saved to /build/reports/${partner}.html`)

// this is used for debugging, and by formatSidebarForSlack

const outDir = path.join(buildDir, 'aggregated-data', partner)
fs.ensureDirSync(outDir)

const writeJSON = (name, data) =>
  fs.writeJSONSync(path.join(outDir, name), data, { spaces: 2 })

writeJSON('byCount.json', reducedCountDataPoints)
writeJSON('byTestCount.json', reducedTestCountDataPoints)
writeJSON('countryCounts.json', countryCounts)
writeJSON('info.json', infoData)

// this meant to be used in Jenkins so it's super simple for a `sh()` call

const fs = require('fs-extra')
const path = require('path')

const dataDir = path.resolve(__dirname, '..', '..', 'data')
const gSheetPath = path.join(dataDir, 'gSheetKnownPartners.json')

if (!fs.existsSync(gSheetPath)) {
  console.log('This you\'re seeing this in Jenkins, something\'s wrong')
  console.log('If there isn\'t a gSheet then data was not processed')
  console.log('Run `gSheetFetchKnownPartners.js` or `npm run data:download`')

  throw new Error('No gSheet file')
}

const data =
  fs.readJSONSync(gSheetPath)
    .filter(obj => obj['partner-report'] === true)
    .map(obj => obj['partner-name'])

// the "return" to sh()
// hope there's never a comma in one of their names...
console.log(data.join(',').trim())

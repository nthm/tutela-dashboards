const path = require('path')
const makeBaseConfig = require('../../webpack.base')

const config = makeBaseConfig({
  tree: path.resolve(__dirname, '..', '..', 'frontends', 'global-country'),
  htmlTitle: 'Global Dashboard',
})

// these need to match script tag in index.ejs in the `entry` directory above
Object.assign(config.externals, {
  datametricCountA: 'window.dataPayloads.metricCountA',
  datametricCountPerColumn: 'window.dataPayloads.metricCountPerColumn',
  dataSidebar: 'window.dataPayloads.sidebar',
})

const buildPath = path.resolve(__dirname, 'build')
console.log('Building into', buildPath)

config.output.path = buildPath
if (process.env.NODE_ENV === 'development') {
  config.devServer.contentBase = buildPath
}

module.exports = config

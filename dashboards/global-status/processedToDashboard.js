const fs = require('fs-extra')
const path = require('path')
const minimist = require('minimist')

const argv = minimist(process.argv.slice(2))

const isoDate = date => date.toISOString().split('T')[0]
const validDate = date => isNaN(Date.parse(date)) === false
const noJSONExt = filename => filename.split('.json')[0]

const buildDir = path.resolve(__dirname, 'build')
const dataDir = path.resolve(__dirname, '..', '..', 'data')
const processedDir = path.join(dataDir, 'processed')

// can easily sort ISO dates
const processedFiles = fs.readdirSync(processedDir).sort()

// fallback values if not told a date range to process
let startDate = noJSONExt(processedFiles[0])
let endDate = noJSONExt(processedFiles[processedFiles.length - 1])

// be less forgiving than run.js
let startThreshold = -Infinity
if (('from' in argv) && validDate(argv.from)) {
  console.log('ISO date --from given. Limiting data range')
  const date = new Date(argv.from)
  startThreshold = date.getTime()
  startDate = isoDate(date)
}
let endThreshold = Infinity
if (('to' in argv) && validDate(argv.to)) {
  console.log('ISO date --to given. Limiting data range')
  const date = new Date(argv.to)
  endThreshold = date.getTime()
  endDate = isoDate(date)
}

console.log()

// there are three files to export as ReCharts format. global device and QoS
// counts, country counts, and sidebar values
const globalCounts = []
const countryCounts = []

// key: country, value: count, as a total. this is used for knowing what
// countries to keep (if their average is greater than a threshold)
const countrySums = new Map()

// key: timestamp, value: object of (key: country, value: count)
// example: [152717...02, { 'Turkey': 201828, 'Brazil': ... }]
const timestampToCountryCountPair = new Map()

// used by integration counts
const dayBeforeEndDate = (() => {
  const date = new Date(endDate)
  date.setDate(date.getDate() - 1)
  return isoDate(date)
})()

let integrationCountLastDay = null
let integrationCountSecondLastDay = null

let previousDate = null
for (const filename of processedFiles) {
  const fileISO = noJSONExt(filename)
  const currentDate = new Date(fileISO)
  const currentTimestamp = currentDate.getTime()

  if (previousDate !== null) {
    const isoPrevious = isoDate(previousDate)
    // move `previous` up. hopefully to `current`, but maybe less
    previousDate.setDate(previousDate.getDate() + 1)
    if (isoDate(previousDate) !== fileISO) {
      console.log(`Warning: Data gap between ${isoPrevious} and ${fileISO}`)
    }
  } else {
    console.log('Starting at', fileISO)
  }
  // move `previous` up to `current`
  previousDate = currentDate
  if (currentTimestamp < startThreshold) {
    // gaps in data are still reported, but otherwise there's silence
    continue
  }
  if (currentTimestamp > endThreshold) {
    console.log(filename, 'is beyond is the end date. Not processing. Exiting.')
    break
  }
  console.log(fileISO)
  const data = require(path.join(processedDir, filename))

  globalCounts.push({
    Date: fileISO,
    Count: data.TotalGlobal.Count,
    TestCount: data.TotalGlobal.TestCount,
  })

  // handle countries
  let liveIntegrations = 0
  Object.values(data.PerCountryAM).forEach(countries => {
    let integrationLive = false
    for (const country in countries) {
      // only handle count for now
      const value = countries[country].Count
      if (value > 0) {
        integrationLive = true
      }
      countrySums.set(country, (countrySums.get(country) || 0) + value)

      const mapEntry = timestampToCountryCountPair.get(fileISO)
      // insert
      if (!mapEntry) {
        timestampToCountryCountPair.set(fileISO, { [country]: value })
        return
      }
      // else, update
      mapEntry[country] = (mapEntry[country] || 0) + value
      timestampToCountryCountPair.set(fileISO, mapEntry)
    }
    if (integrationLive) {
      liveIntegrations++
    }
  })
  // possibly handle integration counts
  if (fileISO === dayBeforeEndDate) {
    integrationCountSecondLastDay = liveIntegrations
  }
  if (fileISO === endDate) {
    integrationCountLastDay = liveIntegrations
  }
}

const days = timestampToCountryCountPair.size
console.log()
console.log(`Processed ${days} days`)
console.log(`Accumulated ${countrySums.size} countries`)
console.log()

const threshold = 20000
// https://stackoverflow.com/q/35940216/delete-from-set-or-map-during-iteration
countrySums.forEach((sum, country) => {
  if (sum / days < threshold) {
    countrySums.delete(country)
  }
})

console.log(`${countrySums.size} countries have an average >= ${threshold}`)
console.log('Keeping those')
console.log()

const allFinalCountries = [...countrySums.keys()]
for (const [iso, countryCountPair] of timestampToCountryCountPair) {
  const result = { Date: iso }
  allFinalCountries.forEach(country => {
    result[country] = countryCountPair[country] || 0
  })
  countryCounts.push(result)
}

// calculate sidebar values

const percentChange = (day, dayBefore) =>
  (((day - dayBefore) / dayBefore) * 100).toFixed(1)

// popping (see below) is destructive so do this reduction first:
const testsPerformed30Days =
  globalCounts.reduce((sum, { TestCount }) => sum + TestCount, 0)

const globalLastDay = globalCounts[globalCounts.length - 1]
const globalSecondLastDay = globalCounts[globalCounts.length - 2]

const sidebar = {
  'dateRange': `${startDate} to ${endDate}`,
  'generatedOn': (new Date()).toUTCString(),
  'stats': [
    // for: Title A
    {
      'value': globalLastDay.Count,
      'change': percentChange(
        globalLastDay.Count,
        globalSecondLastDay.Count
      ),
    },
    // for: Title B
    {
      'value': globalLastDay.TestCount,
      'change': percentChange(
        globalLastDay.TestCount,
        globalSecondLastDay.TestCount
      ),
    },
    // for: Title C
    {
      'value': testsPerformed30Days,
    },
    // for: Title D
    {
      'value': integrationCountLastDay,
      'change': percentChange(
        integrationCountLastDay,
        integrationCountSecondLastDay
      ),
    },
  ],
}

console.log('Merging data into empty dashboard template')
const html = fs.readFileSync(path.join(buildDir, 'index.html'), 'utf8')

const newHtml = html
  .replace(
    'metricCountA:null',
    'metricCountA:' + JSON.stringify(globalCounts)
  )
  .replace(
    'metricCountPerColumn:null',
    'metricCountPerColumn:' + JSON.stringify(countryCounts)
  )
  .replace(
    'sidebar:null',
    'sidebar:' + JSON.stringify(sidebar)
  )

// sanity check
if (newHtml === html) {
  throw new Error('Nothing changed while trying to merge data, bailing')
}

fs.writeFileSync(path.join(buildDir, 'report.html'), newHtml)
console.log('Saved to /build/report.html')

// this is used for debugging, and by formatSidebarForSlack

const outDir = path.join(buildDir, 'aggregated-data')
fs.ensureDirSync(outDir)

const writeJSON = (name, data) =>
  fs.writeJSONSync(path.join(outDir, name), data, { spaces: 2 })

writeJSON('metricCountA.json', globalCounts)
writeJSON('metricCountPerColumn.json', countryCounts)
writeJSON('sidebar.json', sidebar)

import React, { Component } from 'react'
import PropTypes from 'prop-types' // eslint-disable-line
import ReactDOM from 'react-dom'
import { format } from 'd3-format'

import TutLineChart from './components/linechart'
import TutHeader from './components/header'
import TutStats from './components/stats'
import TutMap from './components/map'

// these are external. see webpack config and index.ejs
import infoData from 'dataInfo'
import countryCounts from 'dataCountryCounts'
import byCount from 'metricCount'
import byTestCount from 'metricTestCount'

import './index.css'

const formatter = x => (format('.3s')(x)).replace('G', 'B')

const sortedCountries = !countryCounts
  ? []
  : Object.keys(countryCounts)
    .sort((a, b) =>
      countryCounts[b].TestCount - countryCounts[a].TestCount)
    .slice(0, 20)

const Intro = props =>
  <div
    style={{ marginBottom: 20 }}
  >
    <h3
      style={{ margin: '5px 0' }}
    >
      {props.title}
    </h3>
    <em>
      {props.subtext}
    </em>
  </div>
Intro.propTypes = {
  title: PropTypes.string.isRequired,
  subtext: PropTypes.string.isRequired,
}

class App extends Component {
  state = {
    firstRender: true,
  }

  componentDidMount() {
    const dpi = this.dpiTester.offsetHeight
    console.log('one inch is', dpi)
    this.setState({
      firstRender: false,
      dpi,
    })
  }

  render() {
    const { firstRender, dpi } = this.state
    if (firstRender) {
      console.log('First render, testing DPI')
      return (
        <div
          ref={node => { this.dpiTester = node }}
          style={{
            height: '1in',
            width: '1in',
          }}
        />
      )
    }
    if (!infoData) {
      return (
        <main>
          <TutHeader />
        </main>
      )
    }
    return (
      <main>
        <TutHeader />
        <TutStats
          formatter={formatter}
        />
        <section id='report-left'>
          <Intro
            title='Title One'
            subtext={`Here is ${infoData.days} days of data`}
          />
          <TutLineChart
            width={dpi * 4.2}
            height={dpi * 1.4}
            data={byCount.map(obj => ({
              Date: obj.Date,
              Total: Object.entries(obj)
                .filter(([key]) => key !== 'Date')
                .reduce((sum, [, value]) => sum + value, 0),
            }))}
            lineColor='#ff5259'
          />
          <hr className='hrgap' />
          <Intro
            title='Title Two'
            subtext={`Another subtext`}
          />
          <TutLineChart
            width={dpi * 4.2}
            height={dpi * 2.2}
            data={byCount}
          />
          <hr className='hrgap' />
          <Intro
            title='Count per unit'
            subtext={`Subtext 3`}
          />
          <TutLineChart
            width={dpi * 4.2}
            height={dpi * 2.2}
            data={byTestCount}
          />
        </section>
        <section id='report-right'>
          <Intro
            title='Top countries'
            subtext={`Look at them all`}
          />
          <TutMap
            width={dpi * 4.2}
            height={dpi * 3}
            toRender={sortedCountries}
          />
          <table>
            <thead>
              <tr>
                <th />
                <th />
                <th style={{ textAlign: 'right' }}>{
                  byCount[byCount.length - 1].Date
                }</th>
                <th
                  colSpan='2'
                  style={{ textAlign: 'center' }}
                >
                  {infoData.days} days
                </th>
              </tr>
              <tr>
                <th />
                <th>Country</th>
                <th style={{ textAlign: 'right' }}>Unit</th>
                <th>Avg</th>
                <th>Test Count</th>
              </tr>
            </thead>
            <tbody>
              {sortedCountries.map((name, index) => {
                const {
                  AverageCount,
                  TestCount,
                  LastCount,
                } = countryCounts[name]
                const maybeFormattedLastCount =
                  (!LastCount || LastCount < 100)
                    ? LastCount || '-'
                    : formatter(LastCount)
                return (
                  <tr key={name}>
                    <td>{index + 1}.</td>
                    <td>{name}</td>
                    <td>{maybeFormattedLastCount}</td>
                    <td>{formatter(AverageCount || 0)}</td>
                    <td>{formatter(TestCount || 0)}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
          <div id='info'>
            <p>Generated on: {infoData.generatedOn}</p>
          </div>
        </section>
      </main>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'))

import React, { Component } from 'react'
import PropTypes from 'prop-types' // eslint-disable-line
import * as d3 from 'd3'
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from 'recharts'
import { format as formatter } from 'd3-format'

const IntlFormat = new Intl.DateTimeFormat('en-US', {
  month: 'short', day: 'numeric',
})

class TutLineChart extends Component {
  static propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
    lineColor: PropTypes.string,
  }
  constructor(props) {
    super(props)

    const { data } = this.props
    if (!data) {
      return
    }
    const totals = data.reduce((total, obj) => {
      for (const column in obj) {
        if (column === 'Date') continue
        total[column] = obj[column]
      }
      return total
    }, {})

    const days = data.length
    const averages = {}
    for (const key in totals) {
      averages[key] = totals[key] / days
    }

    this.paletteMap = {}
    this.sortedLegendPayload =
      Object.keys(averages)
        .sort((a, b) => averages[b] - averages[a])
        .reduce((arr, column, index) => {
          const color = d3.schemeCategory10[index]
          this.paletteMap[column] = color
          arr.push({ value: column, type: 'line', id: column, color })
          return arr
        }, [])
  }

  render() {
    const { data, lineColor, ...other } = this.props
    if (!data) {
      return <h1>No data. <code>{JSON.stringify(data)}</code></h1>
    }
    return (
      <LineChart
        data={data}
        style={{ marginLeft: -30 }}
        margin={{ bottom: 5, right: 20 }}
        {...other}
      >
        <XAxis
          dataKey='Date'
          stroke='#fff'
          style={{ fontSize: 10 }}
          height={10}
          tickFormatter={iso => {
            const our = new Date(iso)
            const utc = new Date(our.getTime() + our.getTimezoneOffset() * 6e4)
            return IntlFormat.format(utc)
          }}
          interval={5}
        />
        <YAxis
          stroke='#fff'
          style={{ fontSize: 10 }}
          tickFormatter={formatter('.1s')}
          // padding={{ bottom: 20 }}
          // domain={{ dataMin: 1e6 }}
        />
        <CartesianGrid
          vertical
          horizontal
          stroke='#555'
        />
        <Tooltip
          wrapperStyle={{
            fontSize: 12,
            fontWeight: 'bold',
          }}
          formatter={formatter('.3s')}
          itemSorter={(a, b) => b.value - a.value}
        />
        {!lineColor &&
          <Legend
            margin={{ top: 60 }}
            wrapperStyle={{
              color: '#fff',
              fontSize: 10,
            }}
            layout='vertical'
            payload={this.sortedLegendPayload}
          />
        }
        {
          Object.keys(data[0])
            .filter(x => x !== 'Date')
            .map(packageName =>
              <Line
                key={packageName}
                type='monotone'
                strokeWidth={3}
                dot={false}
                dataKey={packageName}
                stroke={lineColor || this.paletteMap[packageName]}
                activeDot={{ r: 2 }}
              />)
        }
      </LineChart>
    )
  }
}

export default TutLineChart

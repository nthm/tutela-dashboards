import React, { Component } from 'react'
import PropTypes from 'prop-types' // eslint-disable-line
import * as d3 from 'd3'

import worldData from './mapWorldData.json'
import countryCounts from 'dataCountryCounts'

class TutMap extends Component {
  static propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    toRender: PropTypes.array.isRequired,
  }

  componentDidMount() {
    const { width, height, toRender } = this.props
    const projection = d3.geoMercator()
      .scale(width / 2 / Math.PI)
      .translate([width / 2, height / 2])

    this.path =
      d3.geoPath()
        .projection(projection)

    if (countryCounts) {
      const maxCount = d3.max(toRender, name => countryCounts[name].TestCount)
      const minCount = d3.min(toRender, name => countryCounts[name].TestCount)

      this.colorScale =
        d3.scaleLog()
          .domain([minCount * 0.5, maxCount])
          .range(['white', 'red'])
    }

    d3.select('svg#map')
      .append('g')
      .attr('class', 'countries')
      .selectAll('path')
      .data(worldData.features)
      .enter()
      .append('path')
      .attr('fill', obj => {
        if (!this.colorScale) {
          // fallback for no data
          return '#eee'
        }
        const data = countryCounts[obj.properties.name]
        return this.colorScale(data ? data.TestCount : 0)
      })
      .attr('d', this.path)
  }

  render() {
    const { width, height } = this.props
    return (
      <svg
        id='map'
        width={width}
        height={height}
        style={{ margin: '0 0 -60px -20px' }}
      />
    )
  }
}

export default TutMap

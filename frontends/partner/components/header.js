import React from 'react'
import logoSVG from '../../resources/icons/logo.svg'

import infoData from 'dataInfo'

const TutHeader = () =>
  <header>
    <svg
      height='40'
      width='220'
      preserveAspectRatio='none'
    >
      <use xlinkHref={`#${logoSVG.id}`} />
    </svg>
    <em>
      For more information
    </em>
    <div
      style={{
        position: 'absolute',
        top: 10,
        right: 10,
        textAlign: 'right',
      }}
    >
      <h1>{
        infoData
          ? `${infoData.partnerName} Summary`
          : 'No data'
      }</h1>
      <a href='https://tutela.com'>www.tutela.com</a>
    </div>
  </header>

export default TutHeader

import React, { Component } from 'react'
import PropTypes from 'prop-types' // eslint-disable-line
import arrowSVG from '../../resources/icons/arrow.svg'

import infoData from 'dataInfo'
import byCount from 'metricCount'
import byTestCount from 'metricTestCount'

const reduceDataToSum = obj =>
  Object.entries(obj)
    .filter(([key]) => key !== 'Date') // filter to only package names
    .reduce((sum, [, value]) => sum + value, 0)


const percentChange = (day, dayBefore) =>
  (((day - dayBefore) / dayBefore) * 100).toFixed(1)

const red = '#ff4141'
const green = '#00ad00'

class TutStats extends Component {
  static propTypes = {
    formatter: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    const sums = {
      byCount,
      byTestCount,
    }
    for (const [name, dataset] of Object.entries(sums)) {
      const len = dataset.length
      sums[name] = {
        sumMonth: dataset.reduce((sum, obj) => sum + reduceDataToSum(obj), 0),
        sumLastDay: reduceDataToSum(dataset[len - 1]),
        sumTwoDaysAgo: reduceDataToSum(dataset[len - 2]),
      }
    }
    this.state = {}
    this.state.stats = [
      {
        title: 'MetricA',
        data: {
          value: sums.byCount.sumLastDay,
          change: percentChange(
            sums.byCount.sumLastDay,
            sums.byCount.sumTwoDaysAgo),
        },
      },
      {
        title: 'MetricB Yesterday',
        data: {
          value: sums.byTestCount.sumLastDay,
          change: percentChange(
            sums.byTestCount.sumLastDay,
            sums.byTestCount.sumTwoDaysAgo),
        },
      },
      {
        title: `MetricC Over ${infoData.days} Days`,
        data: {
          value: sums.byTestCount.sumMonth,
        },
      },
    ]
  }

  render() {
    const { stats } = this.state
    const { formatter } = this.props
    return (
      <div className='stats'>
        {stats.map(({ title, data }) => {
          const { value, change } = data
          const negative = change < 0
          return (
            <div className='stat' key={title}>
              <h1>{title}</h1>
              <span>
                {formatter(value)}
                {change &&
                  <div
                    style={{ color: negative ? red : green }}
                  >
                    <svg
                      width='20'
                      height='15'
                      transform={`scale(1, ${negative ? -1 : 1})`}
                      fill={negative ? red : green}
                    >
                      <use xlinkHref={`#${arrowSVG.id}`} />
                    </svg>
                    {!negative && '+'}{change}%
                  </div>
                }
              </span>
            </div>
          )
        })}
      </div>
    )
  }
}

export default TutStats

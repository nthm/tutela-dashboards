import React, { Component } from 'react'
import PropTypes from 'prop-types' // eslint-disable-line
import { format as formatter } from 'd3-format'

import LineChart from 'recharts/es6/chart/LineChart'
import Line from 'recharts/es6/cartesian/Line'
import XAxis from 'recharts/es6/cartesian/XAxis'
import YAxis from 'recharts/es6/cartesian/YAxis'
import CartesianGrid from 'recharts/es6/cartesian/CartesianGrid'
import Tooltip from 'recharts/es6/component/Tooltip'
import Legend from 'recharts/es6/component/Legend'
import ResponsiveContainer from 'recharts/es6/component/ResponsiveContainer'

const IntlFormat = new Intl.DateTimeFormat('en-US', {
  month: 'short', day: 'numeric',
})

// used for line chart styling in our custom
const palette = [
  '#f176c3',
  '#fffd8a',
  '#d1f3b2',
  '#7cdfff',
  '#9d9bff',
  '#a2ff00',
  '#ff00a2',
  '#006975',
  '#CD462F',
  '#79AEEE',
]

class metricCountPerColumn extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
  }
  constructor(props) {
    super(props)

    const data = this.props.data
    if (!data) {
      return
    }
    const totals = data.reduce((total, obj) => {
      for (const column in obj) {
        if (column === 'Date') continue
        total[column] = obj[column]
      }
      return total
    }, {})

    const days = data.length
    const averages = {}
    for (const key in totals) {
      averages[key] = totals[key] / days
    }

    this.paletteMap = {}
    this.sortedLegendPayload =
      Object.keys(averages)
        .sort((a, b) => averages[b] - averages[a])
        .reduce((arr, column, index) => {
          const color = palette[index]
          this.paletteMap[column] = color
          arr.push({ value: column, type: 'line', id: column, color })
          return arr
        }, [])
  }
  render() {
    const data = this.props.data
    return (
      <ResponsiveContainer className='row'>
        {!data
          ? <h1>No data. <code>{JSON.stringify(data)}</code></h1>
          : <LineChart
            data={data}
            margin={{ right: 80, bottom: 20 }}
          >
            <Legend payload={this.sortedLegendPayload} />
            <XAxis
              dataKey='Date'
              stroke='#fff'
              style={{ fontSize: 15 }}
              height={10}
              tickFormatter={iso => {
                const our = new Date(iso)
                const utc = new Date(our.getTime() + our.getTimezoneOffset() * 6e4)
                return IntlFormat.format(utc)
              }}
              interval={3}
            />
            <YAxis
              width={70}
              stroke='#fff'
              style={{ fontSize: 15 }}
              tickFormatter={formatter('.3s')}
              domain={['auto', 'auto']}
              scale='log'
            />
            <CartesianGrid
              vertical
              horizontal
              stroke='#555'
            />
            <Tooltip
              wrapperStyle={{
                fontSize: 15,
                fontWeight: 'bold',
              }}
              formatter={formatter('.3s')}
              itemSorter={(a, b) => b.value - a.value}
            />
            {Object.keys(data[0])
              .filter(x => x !== 'Date')
              .map(column =>
                <Line
                  key={column}
                  type='monotone'
                  strokeWidth={3}
                  dot={false}
                  dataKey={column}
                  stroke={this.paletteMap[column]}
                  activeDot={{ r: 2 }}
                />)
            }
          </LineChart>
        }
      </ResponsiveContainer>
    )
  }
}

export default metricCountPerColumn

// this linechart has two overlayed lines with a linear axis on each side

import React from 'react'
import { format as formatter } from 'd3-format'

import LineChart from 'recharts/es6/chart/LineChart'
import Line from 'recharts/es6/cartesian/Line'
import XAxis from 'recharts/es6/cartesian/XAxis'
import YAxis from 'recharts/es6/cartesian/YAxis'
import CartesianGrid from 'recharts/es6/cartesian/CartesianGrid'
import Tooltip from 'recharts/es6/component/Tooltip'
import Legend from 'recharts/es6/component/Legend'
import ResponsiveContainer from 'recharts/es6/component/ResponsiveContainer'

const palette = {
  'TestCount': '#EFB900',
  'Count': '#FF5D5D',
}

const IntlFormat = new Intl.DateTimeFormat('en-US', {
  month: 'short', day: 'numeric',
})

// eslint-disable-next-line react/prop-types
const metricCountA = ({ data }) =>
  <ResponsiveContainer className='row'>
    {!data
      ? <h1>No data. <code>{JSON.stringify(data)}</code></h1>
      : <LineChart
        margin={{ right: 20, bottom: 20 }}
        data={data}
      >
        <Legend />
        <XAxis
          dataKey='Date'
          stroke='#fff'
          style={{ fontSize: 15 }}
          height={10}
          tickFormatter={iso => {
            const our = new Date(iso)
            const utc = new Date(our.getTime() + our.getTimezoneOffset() * 6e4)
            return IntlFormat.format(utc)
          }}
          interval={3}
        />
        <YAxis
          width={70}
          yAxisId='Count'
          label={{
            value: 'Count',
            angle: -90,
            position: 'insideTopLeft',
            viewBox:{ x: 10, y: 105 },
            stroke: '#fff',
            fontSize: 15,
          }}
          stroke='#fff'
          style={{ fontSize: 15 }}
          tickFormatter={formatter('.2s')}
          domain={[0, 'dataMax + 100000']}
        />
        <YAxis
          yAxisId='TestCount'
          label={{
            value: 'Test Count',
            angle: -90,
            position: 'insideTopRight',
            stroke: '#fff',
            fontSize: 15,
          }}
          stroke='#fff'
          style={{ fontSize: 15 }}
          tickFormatter={formatter('.2s')}
          domain={[0, 'dataMax + 100000']}
          orientation='right'
        />
        <CartesianGrid
          vertical
          horizontal
          stroke='#555'
        />
        <Tooltip
          wrapperStyle={{
            fontSize: 15,
            fontWeight: 'bold',
          }}
          formatter={formatter('.3s')}
          itemSorter={(a, b) => b.value - a.value}
        />
        {Object.keys(data[0])
          .filter(x => x !== 'Date')
          .map(column =>
            <Line
              yAxisId={column}
              key={column}
              type='monotone'
              strokeWidth={3}
              dot={false}
              dataKey={column}
              stroke={palette[column]}
              activeDot={{ r: 2 }}
            />)
        }
      </LineChart>
    }
  </ResponsiveContainer>

export default metricCountA

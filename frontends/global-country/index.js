import React from 'react'
import ReactDOM from 'react-dom'

import Stats from './stats'
import metricCountA from './linecharts/metricCountA'
import metricCountPerColumn from './linecharts/metricCountPerColumn'

import logoSVG from '../resources/icons/logo.svg'
import './index.css'

// this is external. see webpack config
import metricCountAData from 'datametricCountA'
import metricCountPerColumnData from 'datametricCountPerColumn'
import sidebarData from 'dataSidebar'

console.log('Data:')
console.log('metricCountAData:', metricCountAData)
console.log('metricCountPerColumnData:', metricCountPerColumnData)
console.log('sidebarData:', sidebarData)

const Dashboard = () =>
  <div className='wrapper'>
    <header>
      <svg
        height='35'
        width='150'
        preserveAspectRatio='none'
      >
        <use xlinkHref={'#' + logoSVG.id}></use>
      </svg>
      <em>For more information</em>
      {sidebarData &&
        <h2 style={{ float: 'right', margin: 0 }}>
          {sidebarData.title}
        </h2>
      }
    </header>
    <section id='sidebar'>
      <Stats data={sidebarData} />
      {sidebarData &&
        <div id='info'>
          <p>Generated On: {sidebarData.generatedOn}</p>
          <p>Date range: {sidebarData.dateRange}</p>
        </div>
      }
    </section>
    <main>
      <metricCountA data={metricCountAData} />
      <metricCountPerColumn data={metricCountPerColumnData} />
    </main>
  </div>

ReactDOM.render(<Dashboard />, document.getElementById('root'))

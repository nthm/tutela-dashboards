import React from 'react'
import { format } from 'd3-format'

import arrowSVG from '../../resources/icons/arrow.svg'

// used for turning '20000' into '20k'
const formatter = format('.3s')

const sidebarTitles = [
  'No peaking :)',
  'No peaking :)',
  'No peaking :)',
  'No peaking :)',
]

// eslint-disable-next-line react/prop-types
const Stats = ({ data }) =>
  <div id='stats'>
    {sidebarTitles.map((text, index) => {
      const row = data
        ? data.stats[index]
        : { value: 0, change: 0 }
      const negative = row.change < 0
      return (
        <div key={index}>
          <h1>{text}</h1>
          <span>
            {row.value > 10000
              ? formatter(row.value).replace('G', 'B') /* SI prefix */
              : format(',')(row.value)
            }
            {'change' in row &&
              <sub>
                <svg
                  transform={`scale(1, ${negative ? -1 : 1})`}
                  fill={negative ? 'red' : 'green'}
                >
                  <use xlinkHref={'#' + arrowSVG.id}></use>
                </svg>
                {!negative && '+'}{row.change}%
              </sub>}
          </span>
        </div>
      )
    })}
  </div>

export default Stats

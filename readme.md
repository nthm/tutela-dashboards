# Dashboards

These were made around August 2018 and are updated daily as a Jenkins task.
There are a few phases for data processing.

This code won't run on its own. It's missing a lot that was removed/sanitized
for security.

I'll refer to this code for Node tasks such as dealing with S3, replacing shell
scripts, generating HTML catalogs, interacting with gSheets, and doing 3-way
diffs of file trees. Will be useful.

Really, it's the first large backend (non-database) project I've done. It needed
to interact with a lot of different servers and facilitate communication between
them while also processing the streams and handling caching of data in both the
filesystem and S3. That's heat.

---

Data is needed to start doing anything. Those are the `npm run data:*` tasks.
It'll look on S3 for already processed data and use that, or pull down raw data
from the required sources, process it, and put it on S3 as cache. It also checks
ETags (S3's way of saying MD5) and uploads if it's different.

This project used to be three seperate repos. After the merge some functionality
was lost - such as hot reloading for frontend development.

No peeking :)

## Country Status

![](./dashboards/country-status/country-screenshot.png)

## Global Status

![](./dashboards/global-status/global-screenshot.png)

## Partner Status

![](./dashboards/partner-status/partner-screenshot.png)

Unlike `node-data-processing`, this does not pull from an S3 bucket. It _should_
but it's not yet setup. Instead it pulls down raw data from the required Tutela
APIs when needed, and processes it. __Someday please update the data processing
to use S3 like Global Status!__

Building one country produces an HTML file that is 500kb. It takes 30 seconds.
All assets are injected except the font which is external. Originally this was
to make "the HTML file distributable" but we only use signed URLs anyway. We
need URLs that can expire. Not a file. What's the point of having it all in
_one_ HTML file again? 500kb * 77 countries * 30 days = __1.1GB__

Not to mention a build takes 25 minutes on Jenkins, where 95% of that is
recompiling the exact same code. The only thing that changes between builds is
the country JSON.

No thanks.

We should just produce JSONs of each country, and then have a seperate build
process that inlines (or uses a fetch to a signed URL; likely inlining) it into
an HTML that calls the public/sign URL'd bundle.

We can use `external` in a webpack config to specify externally resolved
dependencies like this. Also, common node_module code like react, recharts, and
lodash - really anything that's large and common in the bundle analysis.

```html
<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width,initial-scale=1'>
    <link rel='preload' href='/Museo_Slab_100-webfont.woff2' as='font' type='font/woff2' crossorigin='anonymous'>
    <title>Per Country Dashboard</title>
    <script crossorigin src='https://unpkg.com/react@16/cjs/react.production.min.js'></script>
    <script crossorigin src='https://unpkg.com/react-dom@16/cjs/react-dom.production.min.js'></script>
    <script crossorigin src='https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.core.min.js'></script>
    <script crossorigin src='https://cdnjs.cloudflare.com/ajax/libs/recharts/1.1.0/Recharts.min.js'></script>
    <script>
      window.countryDataPayload = {
        ...
      }
    </script>
    <script src='.../bundle.js' defer></script>
  </head>
  <body>
    <div id='#root'>
  </body>
</html>
```

Note that while HTMLWebpackPlugin has support for:

```html
<script>
  window.countryDataPayload = <%= JSON.stringify(htmlWebpackPlugin.options.countryDataPayload) %>
</script>
```

I don't want to invoke webpack more than once, and have it do something other
than the main bundle build on different runs. I think a data-helper script that
reads the HTML, does a simple find and replace with a JSON.stringify is best.

The trouble will be signing the URL for the bundle. This can be done in Node,
sure, but can also be done in Jenkins far more easily at the expense that it
will not longer be easy to build locally... The solution is to have another
data-helper that _only_ runs on Jenkins, to replace the bundle.js with a signed
URL that it receives via a CLI argument.

Ok this brings the bundle size from 575kb to 425kb.
